
/* OPCODEs */
#define OPCODE_SPECIAL      0x00
#define OPCODE_REGIMM       0x01
#define OPCODE_J            0x02
#define OPCODE_JAL          0x03
#define OPCODE_BEQ          0x04
#define OPCODE_BNE          0x05
#define OPCODE_BLEZ         0x06
#define OPCODE_BGTZ         0x07

#define OPCODE_ADDI         0x08    // DEPRECATED
#define OPCODE_ADDIU        0x09
#define OPCODE_SLTI         0x0A
#define OPCODE_SLTIU        0x0B
#define OPCODE_ANDI         0x0C
#define OPCODE_ORI          0x0D
#define OPCODE_XORI         0x0E
#define OPCODE_LUI          0x0F

#define OPCODE_BEQL         0x14    // DEPRECATED
#define OPCODE_BNEL         0x15    // DEPRECATED
#define OPCODE_BLEZL        0x16    // DEPRECATED
#define OPCODE_BGTZL        0x17    // DEPRECATED

#define OPCODE_DADDI        0x18    // DEPRECATED
#define OPCODE_DADDIU       0x19
#define OPCODE_LDL          0x1A    // DEPRECATED
#define OPCODE_LDR          0x1B    // DEPRECATED
#define OPCODE_SPECIAL2     0x1C
#define OPCODE_JALX         0x1D
#define OPCODE_MAS          0x1E
#define OPCODE_SPECIAL3     0x1F

#define OPCODE_LB           0x20
#define OPCODE_LH           0x21
#define OPCODE_LWL          0x22    // DEPRECATED
#define OPCODE_LW           0x23
#define OPCODE_LBU          0x24
#define OPCODE_LHU          0x25
#define OPCODE_LWR          0x26    // DEPRECATED
#define OPCODE_LWU          0x27

#define OPCODE_SB           0x28
#define OPCODE_SH           0x29
#define OPCODE_SWL          0x2A    // DEPRECATED
#define OPCODE_SW           0x2B
#define OPCODE_SDL          0x2C    // DEPRECATED
#define OPCODE_SDR          0x2D    // DEPRECATED
#define OPCODE_SWR          0x2E    // DEPRECATED
#define OPCODE_CACHE        0x2F    // DEPRECATED

#define OPCODE_LL           0x30    // DEPRECATED
#define OPCODE_LWC1         0x31
#define OPCODE_LWC2         0x32    // DEPRECATED
#define OPCODE_PREF         0x33    // DEPRECATED
#define OPCODE_LLD          0x34    // DEPRECATED
#define OPCODE_LDC1         0x35
#define OPCODE_LDC2         0x36    // DEPRECATED
#define OPCODE_LD           0x37

#define OPCODE_SC           0x38    // DEPRECATED
#define OPCODE_SWC1         0x39
#define OPCODE_SWC2         0x3A    // DEPRECATED
#define OPCODE_PCREL        0x3B
#define OPCODE_SCD          0x3C    // DEPRECATED
#define OPCODE_SDC1         0x3D
#define OPCODE_SDC2         0x3E    // DEPRECATED
#define OPCODE_SD           0x3F

/* SPECIAL FUNCTIONs */
#define SPECIAL_SLL           0x00
#define SPECIAL_MOVCI         0x01
#define SPECIAL_SRL           0x02
#define SPECIAL_SRA           0x03
#define SPECIAL_SLLV          0x04
#define SPECIAL_LSA           0x05
#define SPECIAL_SRLV          0x06
#define SPECIAL_SRAV          0x07

#define SPECIAL_JR            0x08
#define SPECIAL_JALR          0x09
#define SPECIAL_MOVZ          0x0A
#define SPECIAL_MOVN          0x0B
#define SPECIAL_SYSCALL       0x0C
#define SPECIAL_BREAK         0x0D
#define SPECIAL_SDBBP         0x0E
#define SPECIAL_SYNC          0x0F

#define SPECIAL_MFHI          0x10
#define SPECIAL_MTHI          0x11
#define SPECIAL_MFLO          0x12
#define SPECIAL_MTLO          0x13
#define SPECIAL_DSLLV         0x14
#define SPECIAL_DLSA          0x15
#define SPECIAL_DSRLV         0x16
#define SPECIAL_DSRAV         0x17

#define SPECIAL_MULT          0x18
#define SPECIAL_MULTU         0x19
#define SPECIAL_DIV           0x1A
#define SPECIAL_DIVU          0x1B
#define SPECIAL_DMULT         0x1C
#define SPECIAL_DMULTU        0x1D
#define SPECIAL_DDIV          0x1E
#define SPECIAL_DDIVU         0x1F

#define SPECIAL_ADD           0x20
#define SPECIAL_ADDU          0x21
#define SPECIAL_SUB           0x22
#define SPECIAL_SUBU          0x23
#define SPECIAL_AND           0x24
#define SPECIAL_OR            0x25
#define SPECIAL_XOR           0x26
#define SPECIAL_NOR           0x27

#define SPECIAL_SLT           0x2A
#define SPECIAL_SLTU          0x2B
#define SPECIAL_DADD          0x2C
#define SPECIAL_DADDU         0x2D
#define SPECIAL_DSUB          0x2E
#define SPECIAL_DSUBU         0x2F

#define SPECIAL_TGE           0x30
#define SPECIAL_TGEU          0x31
#define SPECIAL_TLT           0x32
#define SPECIAL_TLTU          0x33
#define SPECIAL_TEQ           0x34
#define SPECIAL_SELEQZ        0x35
#define SPECIAL_TNE           0x36
#define SPECIAL_SELNEZ        0x37

#define SPECIAL_DSLL          0x38
#define SPECIAL_DSRL          0x3A
#define SPECIAL_DSRA          0x3B
#define SPECIAL_DSLL32        0x3C
#define SPECIAL_DSRL32        0x3E
#define SPECIAL_DSRA32        0x3F

/* REGIMM RTs */
#define REGIMM_BLTZ      0x00
#define REGIMM_BGEZ      0x01
#define REGIMM_BLTZL     0x02
#define REGIMM_BGEZL     0x03
#define REGIMM_DAHI      0x06

#define REGIMM_TGEI      0x08
#define REGIMM_TGEIU     0x09
#define REGIMM_TLTI      0x0A
#define REGIMM_TLTIU     0x0B
#define REGIMM_TEQI      0x0C
#define REGIMM_TNEI      0x0E

#define REGIMM_BLTZAL    0x10
#define REGIMM_BGEZAL    0x11
#define REGIMM_BLTZALL   0x12
#define REGIMM_BGEZALL   0x13
#define REGIMM_SIGRIE    0x17

#define REGIMM_DATI      0x1E
#define REGIMM_SYNCI     0x1F
