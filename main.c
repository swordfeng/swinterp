#include "cpu.h"
#include <stdint.h>
#include <stdio.h>
#include <limits.h>

uint32_t prog32[] = {
0x27bdfff8,
0xafbe0004,
0x03a0f021,
0xafc40008,
0x8fc20008,
0x8c420000,
0x24430001,
0x8fc20008,
0xac430000,
0x00000000,
0x03c0e821,
0x8fbe0004,
0x27bd0008,
0x03e00008,
0x00000000,
};

uint32_t prog64[] = {
0x8c830000,
0x24630001,
0x03e00008,
0xac830000,
};

#if __WORDSIZE == 32
#define prog prog32
#elif __WORDSIZE == 64
#define prog prog64
#else
#error "unsupported word size"
#endif

int main() {
    volatile int i = 0;
    get_reg()[4] = (uintptr_t)&i;
    exec(prog);
    if (i == 1) printf("good!\n");
    return 0;
}
