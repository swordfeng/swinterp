#include <stdint.h>
uint32_t code[] = {
// 0000000000000000 <quicksort>:
0x67bdff60,    // daddiu	sp,sp,-160
0x00a0c02d,    // move	t8,a1
0xffbf0098,    // sd	ra,152(sp)
0xffbe0090,    // sd	s8,144(sp)
0xffb70088,    // sd	s7,136(sp)
0xffb60080,    // sd	s6,128(sp)
0xffb50078,    // sd	s5,120(sp)
0xffb40070,    // sd	s4,112(sp)
0xffb30068,    // sd	s3,104(sp)
0xffb20060,    // sd	s2,96(sp)
0xffb10058,    // sd	s1,88(sp)
0xffb00050,    // sd	s0,80(sp)
0x0304102f,    // dsubu	v0,t8,a0

// 0000000000000034 <$L240>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0300482d,    // move	a5,t8
0x0080682d,    // move	t1,a0

// 0000000000000058 <$L2>:
0x012d102b,    // sltu	v0,a5,t1
0x14400017,    // bnez	v0,bc <$L202>
0x0089102b,    // sltu	v0,a0,a5
0x8da50000,    // lw	a1,0(t1)
0x00a3102a,    // slt	v0,a1,v1
0x5040001a,    // beqzl	v0,d8 <$L174>
0x8d220000,    // lw	v0,0(a5)
0x65ad0004,    // daddiu	t1,t1,4

// 0000000000000078 <$L203>:
0x8da50000,    // lw	a1,0(t1)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,78 <$L203>
0x65ad0004,    // daddiu	t1,t1,4
0x8d220000,    // lw	v0,0(a5)
0x0062302a,    // slt	a2,v1,v0
0x10c00007,    // beqz	a2,b0 <$L204>
0x012d302b,    // sltu	a2,a5,t1
0x6529fffc,    // daddiu	a5,a5,-4

// 000000000000009c <$L205>:
0x8d220000,    // lw	v0,0(a5)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,9c <$L205>
0x6529fffc,    // daddiu	a5,a5,-4
0x012d302b,    // sltu	a2,a5,t1

// 00000000000000b0 <$L204>:
0x50c0000c,    // beqzl	a2,e4 <$L206>
0x6529fffc,    // daddiu	a5,a5,-4
0x0089102b,    // sltu	v0,a0,a5

// 00000000000000bc <$L202>:
0x1440002b,    // bnez	v0,16c <$L207>
0x0124102f,    // dsubu	v0,a5,a0

// 00000000000000c4 <$L21>:
0x01b8102b,    // sltu	v0,t1,t8
0x1040000a,    // beqz	v0,f4 <$L176>
0x01a0202d,    // move	a0,t1
0x1000ffd8,    // b	34 <$L240>
0x0304102f,    // dsubu	v0,t8,a0

// 00000000000000d8 <$L174>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ffef,    // bnez	a2,9c <$L205>
0x6529fffc,    // daddiu	a5,a5,-4

// 00000000000000e4 <$L206>:
0xada20000,    // sw	v0,0(t1)
0xad250004,    // sw	a1,4(a5)
0x1000ffda,    // b	58 <$L2>
0x65ad0004,    // daddiu	t1,t1,4

// 00000000000000f4 <$L176>:
0xdfbf0098,    // ld	ra,152(sp)
0xdfbe0090,    // ld	s8,144(sp)
0xdfb70088,    // ld	s7,136(sp)
0xdfb60080,    // ld	s6,128(sp)
0xdfb50078,    // ld	s5,120(sp)
0xdfb40070,    // ld	s4,112(sp)
0xdfb30068,    // ld	s3,104(sp)
0xdfb20060,    // ld	s2,96(sp)
0xdfb10058,    // ld	s1,88(sp)
0xdfb00050,    // ld	s0,80(sp)
0x03e00008,    // jr	ra
0x67bd00a0,    // daddiu	sp,sp,160

// 0000000000000124 <$L178>:
0x0062302a,    // slt	a2,v1,v0
0x10c001b5,    // beqz	a2,800 <$L14>
0x014e302b,    // sltu	a2,a6,t2
0x654afffc,    // daddiu	a6,a6,-4

// 0000000000000134 <$L208>:
0x8d420000,    // lw	v0,0(a6)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,134 <$L208>
0x654afffc,    // daddiu	a6,a6,-4
0x014e302b,    // sltu	a2,a6,t2
0x50c00022,    // beqzl	a2,1d4 <$L201>
0x654afffc,    // daddiu	a6,a6,-4
0x008a102b,    // sltu	v0,a0,a6

// 0000000000000154 <$L210>:
0x14400036,    // bnez	v0,230 <$L209>
0x0144102f,    // dsubu	v0,a6,a0

// 000000000000015c <$L31>:
0x01c9102b,    // sltu	v0,t2,a5
0x1040ffd8,    // beqz	v0,c4 <$L21>
0x012e102f,    // dsubu	v0,a5,t2
0x01c0202d,    // move	a0,t2

// 000000000000016c <$L207>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0120502d,    // move	a6,a5
0x0080702d,    // move	t2,a0

// 0000000000000190 <$L12>:
0x014e102b,    // sltu	v0,a6,t2
0x1440ffef,    // bnez	v0,154 <$L210>
0x008a102b,    // sltu	v0,a0,a6
0x8dc50000,    // lw	a1,0(t2)
0x00a3102a,    // slt	v0,a1,v1
0x50400008,    // beqzl	v0,1c8 <$L177>
0x8d420000,    // lw	v0,0(a6)
0x65ce0004,    // daddiu	t2,t2,4

// 00000000000001b0 <$L211>:
0x8dc50000,    // lw	a1,0(t2)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,1b0 <$L211>
0x65ce0004,    // daddiu	t2,t2,4
0x1000ffd8,    // b	124 <$L178>
0x8d420000,    // lw	v0,0(a6)

// 00000000000001c8 <$L177>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ffd9,    // bnez	a2,134 <$L208>
0x654afffc,    // daddiu	a6,a6,-4

// 00000000000001d4 <$L201>:
0xadc20000,    // sw	v0,0(t2)
0xad450004,    // sw	a1,4(a6)
0x1000ffec,    // b	190 <$L12>
0x65ce0004,    // daddiu	t2,t2,4
0x00000000,    // nop

// 00000000000001e8 <$L180>:
0x0062302a,    // slt	a2,v1,v0
0x10c00027,    // beqz	a2,28c <$L24>
0x016f302b,    // sltu	a2,a7,t3
0x656bfffc,    // daddiu	a7,a7,-4

// 00000000000001f8 <$L212>:
0x8d620000,    // lw	v0,0(a7)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,1f8 <$L212>
0x656bfffc,    // daddiu	a7,a7,-4
0x016f302b,    // sltu	a2,a7,t3
0x50c00022,    // beqzl	a2,298 <$L194>
0x656bfffc,    // daddiu	a7,a7,-4
0x008b102b,    // sltu	v0,a0,a7

// 0000000000000218 <$L214>:
0x1440003b,    // bnez	v0,308 <$L213>
0x0164102f,    // dsubu	v0,a7,a0

// 0000000000000220 <$L41>:
0x01ea102b,    // sltu	v0,t3,a6
0x1040ffcd,    // beqz	v0,15c <$L31>
0x014f102f,    // dsubu	v0,a6,t3
0x01e0202d,    // move	a0,t3

// 0000000000000230 <$L209>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0140582d,    // move	a7,a6
0x0080782d,    // move	t3,a0

// 0000000000000254 <$L22>:
0x016f102b,    // sltu	v0,a7,t3
0x1440ffef,    // bnez	v0,218 <$L214>
0x008b102b,    // sltu	v0,a0,a7
0x8de50000,    // lw	a1,0(t3)
0x00a3102a,    // slt	v0,a1,v1
0x5040000f,    // beqzl	v0,2a8 <$L179>
0x8d620000,    // lw	v0,0(a7)
0x65ef0004,    // daddiu	t3,t3,4

// 0000000000000274 <$L215>:
0x8de50000,    // lw	a1,0(t3)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,274 <$L215>
0x65ef0004,    // daddiu	t3,t3,4
0x1000ffd8,    // b	1e8 <$L180>
0x8d620000,    // lw	v0,0(a7)

// 000000000000028c <$L24>:
0x54c0ffe2,    // bnezl	a2,218 <$L214>
0x008b102b,    // sltu	v0,a0,a7
0x656bfffc,    // daddiu	a7,a7,-4

// 0000000000000298 <$L194>:
0xade20000,    // sw	v0,0(t3)

// 000000000000029c <$L241>:
0xad650004,    // sw	a1,4(a7)
0x1000ffec,    // b	254 <$L22>
0x65ef0004,    // daddiu	t3,t3,4

// 00000000000002a8 <$L179>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ffd2,    // bnez	a2,1f8 <$L212>
0x656bfffc,    // daddiu	a7,a7,-4
0x1000fff9,    // b	29c <$L241>
0xade20000,    // sw	v0,0(t3)
0x00000000,    // nop

// 00000000000002c0 <$L182>:
0x0062302a,    // slt	a2,v1,v0
0x10c00027,    // beqz	a2,364 <$L34>
0x010c302b,    // sltu	a2,a4,t0
0x6508fffc,    // daddiu	a4,a4,-4

// 00000000000002d0 <$L216>:
0x8d020000,    // lw	v0,0(a4)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,2d0 <$L216>
0x6508fffc,    // daddiu	a4,a4,-4
0x010c302b,    // sltu	a2,a4,t0
0x50c00022,    // beqzl	a2,370 <$L200>
0x6508fffc,    // daddiu	a4,a4,-4
0x0088102b,    // sltu	v0,a0,a4

// 00000000000002f0 <$L218>:
0x14400034,    // bnez	v0,3c4 <$L217>
0x0104102f,    // dsubu	v0,a4,a0

// 00000000000002f8 <$L51>:
0x018b102b,    // sltu	v0,t0,a7
0x1040ffc8,    // beqz	v0,220 <$L41>
0x016c102f,    // dsubu	v0,a7,t0
0x0180202d,    // move	a0,t0

// 0000000000000308 <$L213>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0160402d,    // move	a4,a7
0x0080602d,    // move	t0,a0

// 000000000000032c <$L32>:
0x010c102b,    // sltu	v0,a4,t0
0x1440ffef,    // bnez	v0,2f0 <$L218>
0x0088102b,    // sltu	v0,a0,a4
0x8d850000,    // lw	a1,0(t0)
0x00a3102a,    // slt	v0,a1,v1
0x5040012a,    // beqzl	v0,7ec <$L181>
0x8d020000,    // lw	v0,0(a4)
0x658c0004,    // daddiu	t0,t0,4

// 000000000000034c <$L219>:
0x8d850000,    // lw	a1,0(t0)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,34c <$L219>
0x658c0004,    // daddiu	t0,t0,4
0x1000ffd8,    // b	2c0 <$L182>
0x8d020000,    // lw	v0,0(a4)

// 0000000000000364 <$L34>:
0x54c0ffe2,    // bnezl	a2,2f0 <$L218>
0x0088102b,    // sltu	v0,a0,a4
0x6508fffc,    // daddiu	a4,a4,-4

// 0000000000000370 <$L200>:
0xad820000,    // sw	v0,0(t0)

// 0000000000000374 <$L246>:
0xad050004,    // sw	a1,4(a4)
0x1000ffec,    // b	32c <$L32>
0x658c0004,    // daddiu	t0,t0,4

// 0000000000000380 <$L183>:
0x0062302a,    // slt	a2,v1,v0
0x10c00110,    // beqz	a2,7c8 <$L220>
0x66f7fffc,    // daddiu	s7,s7,-4

// 000000000000038c <$L221>:
0x8ee20000,    // lw	v0,0(s7)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,38c <$L221>
0x66f7fffc,    // daddiu	s7,s7,-4
0x02fe302b,    // sltu	a2,s7,s8

// 00000000000003a0 <$L195>:
0x50c00109,    // beqzl	a2,7c8 <$L220>
0x66f7fffc,    // daddiu	s7,s7,-4
0x0097102b,    // sltu	v0,a0,s7

// 00000000000003ac <$L223>:
0x14400032,    // bnez	v0,478 <$L222>
0x02e4102f,    // dsubu	v0,s7,a0

// 00000000000003b4 <$L61>:
0x03c8102b,    // sltu	v0,s8,a4
0x1040ffcf,    // beqz	v0,2f8 <$L51>
0x011e102f,    // dsubu	v0,a4,s8
0x03c0202d,    // move	a0,s8

// 00000000000003c4 <$L217>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0100b82d,    // move	s7,a4
0x0080f02d,    // move	s8,a0

// 00000000000003e8 <$L42>:
0x02fe102b,    // sltu	v0,s7,s8
0x1440ffef,    // bnez	v0,3ac <$L223>
0x0097102b,    // sltu	v0,a0,s7
0x8fc50000,    // lw	a1,0(s8)
0x00a3102a,    // slt	v0,a1,v1
0x5040ffe0,    // beqzl	v0,380 <$L183>
0x8ee20000,    // lw	v0,0(s7)
0x67de0004,    // daddiu	s8,s8,4

// 0000000000000408 <$L224>:
0x8fc50000,    // lw	a1,0(s8)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,408 <$L224>
0x67de0004,    // daddiu	s8,s8,4
0x8ee20000,    // lw	v0,0(s7)
0x0062302a,    // slt	a2,v1,v0
0x54c0ffda,    // bnezl	a2,38c <$L221>
0x66f7fffc,    // daddiu	s7,s7,-4
0x1000ffdd,    // b	3a0 <$L195>
0x02fe302b,    // sltu	a2,s7,s8

// 0000000000000430 <$L186>:
0x0062302a,    // slt	a2,v1,v0
0x10c000d8,    // beqz	a2,798 <$L54>
0x02b6302b,    // sltu	a2,s5,s6
0x66b5fffc,    // daddiu	s5,s5,-4

// 0000000000000440 <$L225>:
0x8ea20000,    // lw	v0,0(s5)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,440 <$L225>
0x66b5fffc,    // daddiu	s5,s5,-4
0x02b6302b,    // sltu	a2,s5,s6
0x50c000d3,    // beqzl	a2,7a4 <$L199>
0x66b5fffc,    // daddiu	s5,s5,-4
0x0095102b,    // sltu	v0,a0,s5

// 0000000000000460 <$L227>:
0x1440002f,    // bnez	v0,520 <$L226>
0x02a4102f,    // dsubu	v0,s5,a0

// 0000000000000468 <$L71>:
0x02d7102b,    // sltu	v0,s6,s7
0x1040ffd1,    // beqz	v0,3b4 <$L61>
0x02f6102f,    // dsubu	v0,s7,s6
0x02c0202d,    // move	a0,s6

// 0000000000000478 <$L222>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x02e0a82d,    // move	s5,s7
0x0080b02d,    // move	s6,a0

// 000000000000049c <$L52>:
0x02b6102b,    // sltu	v0,s5,s6
0x1440ffef,    // bnez	v0,460 <$L227>
0x0095102b,    // sltu	v0,a0,s5
0x8ec50000,    // lw	a1,0(s6)
0x00a3102a,    // slt	v0,a1,v1
0x504000c9,    // beqzl	v0,7d8 <$L185>
0x8ea20000,    // lw	v0,0(s5)
0x66d60004,    // daddiu	s6,s6,4
0x00000000,    // nop

// 00000000000004c0 <$L228>:
0x8ec50000,    // lw	a1,0(s6)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,4c0 <$L228>
0x66d60004,    // daddiu	s6,s6,4
0x1000ffd7,    // b	430 <$L186>
0x8ea20000,    // lw	v0,0(s5)

// 00000000000004d8 <$L188>:
0x0062302a,    // slt	a2,v1,v0
0x10c0009d,    // beqz	a2,754 <$L64>
0x0274302b,    // sltu	a2,s3,s4
0x6673fffc,    // daddiu	s3,s3,-4

// 00000000000004e8 <$L229>:
0x8e620000,    // lw	v0,0(s3)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,4e8 <$L229>
0x6673fffc,    // daddiu	s3,s3,-4
0x0274302b,    // sltu	a2,s3,s4
0x50c00098,    // beqzl	a2,760 <$L198>
0x6673fffc,    // daddiu	s3,s3,-4
0x0093102b,    // sltu	v0,a0,s3

// 0000000000000508 <$L231>:
0x1440002f,    // bnez	v0,5c8 <$L230>
0x0264102f,    // dsubu	v0,s3,a0

// 0000000000000510 <$L81>:
0x0295102b,    // sltu	v0,s4,s5
0x1040ffd4,    // beqz	v0,468 <$L71>
0x02b4102f,    // dsubu	v0,s5,s4
0x0280202d,    // move	a0,s4

// 0000000000000520 <$L226>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x02a0982d,    // move	s3,s5
0x0080a02d,    // move	s4,a0

// 0000000000000544 <$L62>:
0x0274102b,    // sltu	v0,s3,s4
0x1440ffef,    // bnez	v0,508 <$L231>
0x0093102b,    // sltu	v0,a0,s3
0x8e850000,    // lw	a1,0(s4)
0x00a3102a,    // slt	v0,a1,v1
0x50400096,    // beqzl	v0,7b4 <$L187>
0x8e620000,    // lw	v0,0(s3)
0x66940004,    // daddiu	s4,s4,4
0x00000000,    // nop

// 0000000000000568 <$L232>:
0x8e850000,    // lw	a1,0(s4)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,568 <$L232>
0x66940004,    // daddiu	s4,s4,4
0x1000ffd7,    // b	4d8 <$L188>
0x8e620000,    // lw	v0,0(s3)

// 0000000000000580 <$L190>:
0x0062302a,    // slt	a2,v1,v0
0x10c00059,    // beqz	a2,6ec <$L74>
0x0232302b,    // sltu	a2,s1,s2
0x6631fffc,    // daddiu	s1,s1,-4

// 0000000000000590 <$L233>:
0x8e220000,    // lw	v0,0(s1)
0x0062302a,    // slt	a2,v1,v0
0x54c0fffd,    // bnezl	a2,590 <$L233>
0x6631fffc,    // daddiu	s1,s1,-4
0x0232302b,    // sltu	a2,s1,s2
0x50c00054,    // beqzl	a2,6f8 <$L197>
0x6631fffc,    // daddiu	s1,s1,-4
0x0091102b,    // sltu	v0,a0,s1

// 00000000000005b0 <$L235>:
0x1440002f,    // bnez	v0,670 <$L234>
0x0224102f,    // dsubu	v0,s1,a0

// 00000000000005b8 <$L91>:
0x0253102b,    // sltu	v0,s2,s3
0x1040ffd4,    // beqz	v0,510 <$L81>
0x0272102f,    // dsubu	v0,s3,s2
0x0240202d,    // move	a0,s2

// 00000000000005c8 <$L230>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0260882d,    // move	s1,s3
0x0080902d,    // move	s2,a0

// 00000000000005ec <$L72>:
0x0232102b,    // sltu	v0,s1,s2
0x1440ffef,    // bnez	v0,5b0 <$L235>
0x0091102b,    // sltu	v0,a0,s1
0x8e450000,    // lw	a1,0(s2)
0x00a3102a,    // slt	v0,a1,v1
0x50400060,    // beqzl	v0,784 <$L189>
0x8e220000,    // lw	v0,0(s1)
0x66520004,    // daddiu	s2,s2,4
0x00000000,    // nop

// 0000000000000610 <$L236>:
0x8e450000,    // lw	a1,0(s2)
0x00a3102a,    // slt	v0,a1,v1
0x5440fffd,    // bnezl	v0,610 <$L236>
0x66520004,    // daddiu	s2,s2,4
0x1000ffd7,    // b	580 <$L190>
0x8e220000,    // lw	v0,0(s1)

// 0000000000000628 <$L193>:
0x0062382a,    // slt	a3,v1,v0
0x10e00028,    // beqz	a3,6d0 <$L84>
0x00b0382b,    // sltu	a3,a1,s0
0x64a5fffc,    // daddiu	a1,a1,-4

// 0000000000000638 <$L237>:
0x8ca20000,    // lw	v0,0(a1)
0x0062382a,    // slt	a3,v1,v0
0x54e0fffd,    // bnezl	a3,638 <$L237>
0x64a5fffc,    // daddiu	a1,a1,-4
0x00b0382b,    // sltu	a3,a1,s0
0x50e00023,    // beqzl	a3,6dc <$L196>
0x64a5fffc,    // daddiu	a1,a1,-4
0x0085102b,    // sltu	v0,a0,a1

// 0000000000000658 <$L238>:
0x5440002b,    // bnezl	v0,708 <$L191>
0xffb80040,    // sd	t8,64(sp)

// 0000000000000660 <$L89>:
0x0211102b,    // sltu	v0,s0,s1
0x1040ffd4,    // beqz	v0,5b8 <$L91>
0x0230102f,    // dsubu	v0,s1,s0
0x0200202d,    // move	a0,s0

// 0000000000000670 <$L234>:
0x000218bb,    // dsra	v1,v0,0x2
0x000317fe,    // dsrl32	v0,v1,0x1f
0x0043102d,    // daddu	v0,v0,v1
0x0002107b,    // dsra	v0,v0,0x1
0x000210b8,    // dsll	v0,v0,0x2
0x0082102d,    // daddu	v0,a0,v0
0x8c430000,    // lw	v1,0(v0)
0x0220282d,    // move	a1,s1
0x0080802d,    // move	s0,a0

// 0000000000000694 <$L82>:
0x00b0102b,    // sltu	v0,a1,s0
0x1440ffef,    // bnez	v0,658 <$L238>
0x0085102b,    // sltu	v0,a0,a1
0x8e060000,    // lw	a2,0(s0)
0x00c3102a,    // slt	v0,a2,v1
0x50400031,    // beqzl	v0,770 <$L192>
0x8ca20000,    // lw	v0,0(a1)
0x66100004,    // daddiu	s0,s0,4
0x00000000,    // nop

// 00000000000006b8 <$L239>:
0x8e060000,    // lw	a2,0(s0)
0x00c3102a,    // slt	v0,a2,v1
0x5440fffd,    // bnezl	v0,6b8 <$L239>
0x66100004,    // daddiu	s0,s0,4
0x1000ffd7,    // b	628 <$L193>
0x8ca20000,    // lw	v0,0(a1)

// 00000000000006d0 <$L84>:
0x54e0ffe1,    // bnezl	a3,658 <$L238>
0x0085102b,    // sltu	v0,a0,a1
0x64a5fffc,    // daddiu	a1,a1,-4

// 00000000000006dc <$L196>:
0xae020000,    // sw	v0,0(s0)

// 00000000000006e0 <$L242>:
0xaca60004,    // sw	a2,4(a1)
0x1000ffeb,    // b	694 <$L82>
0x66100004,    // daddiu	s0,s0,4

// 00000000000006ec <$L74>:
0x54c0ffb0,    // bnezl	a2,5b0 <$L235>
0x0091102b,    // sltu	v0,a0,s1
0x6631fffc,    // daddiu	s1,s1,-4

// 00000000000006f8 <$L197>:
0xae420000,    // sw	v0,0(s2)

// 00000000000006fc <$L243>:
0xae250004,    // sw	a1,4(s1)
0x1000ffba,    // b	5ec <$L72>
0x66520004,    // daddiu	s2,s2,4

// 0000000000000708 <$L191>:
0xffa80038,    // sd	a4,56(sp)
0xffab0030,    // sd	a7,48(sp)
0xffaf0028,    // sd	t3,40(sp)
0xffaa0020,    // sd	a6,32(sp)
0xffae0018,    // sd	t2,24(sp)
0xffa90010,    // sd	a5,16(sp)
0xffac0008,    // sd	t0,8(sp)
0x0c000000,    // jal	0 <quicksort>
0xffad0000,    // sd	t1,0(sp)
0xdfb80040,    // ld	t8,64(sp)
0xdfa80038,    // ld	a4,56(sp)
0xdfab0030,    // ld	a7,48(sp)
0xdfaf0028,    // ld	t3,40(sp)
0xdfaa0020,    // ld	a6,32(sp)
0xdfae0018,    // ld	t2,24(sp)
0xdfa90010,    // ld	a5,16(sp)
0xdfac0008,    // ld	t0,8(sp)
0x1000ffc4,    // b	660 <$L89>
0xdfad0000,    // ld	t1,0(sp)

// 0000000000000754 <$L64>:
0x54c0ff6c,    // bnezl	a2,508 <$L231>
0x0093102b,    // sltu	v0,a0,s3
0x6673fffc,    // daddiu	s3,s3,-4

// 0000000000000760 <$L198>:
0xae820000,    // sw	v0,0(s4)

// 0000000000000764 <$L244>:
0xae650004,    // sw	a1,4(s3)
0x1000ff76,    // b	544 <$L62>
0x66940004,    // daddiu	s4,s4,4

// 0000000000000770 <$L192>:
0x0062382a,    // slt	a3,v1,v0
0x14e0ffb0,    // bnez	a3,638 <$L237>
0x64a5fffc,    // daddiu	a1,a1,-4
0x1000ffd8,    // b	6e0 <$L242>
0xae020000,    // sw	v0,0(s0)

// 0000000000000784 <$L189>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ff81,    // bnez	a2,590 <$L233>
0x6631fffc,    // daddiu	s1,s1,-4
0x1000ffda,    // b	6fc <$L243>
0xae420000,    // sw	v0,0(s2)

// 0000000000000798 <$L54>:
0x54c0ff31,    // bnezl	a2,460 <$L227>
0x0095102b,    // sltu	v0,a0,s5
0x66b5fffc,    // daddiu	s5,s5,-4

// 00000000000007a4 <$L199>:
0xaec20000,    // sw	v0,0(s6)

// 00000000000007a8 <$L245>:
0xaea50004,    // sw	a1,4(s5)
0x1000ff3b,    // b	49c <$L52>
0x66d60004,    // daddiu	s6,s6,4

// 00000000000007b4 <$L187>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ff4b,    // bnez	a2,4e8 <$L229>
0x6673fffc,    // daddiu	s3,s3,-4
0x1000ffe8,    // b	764 <$L244>
0xae820000,    // sw	v0,0(s4)

// 00000000000007c8 <$L220>:
0xafc20000,    // sw	v0,0(s8)
0xaee50004,    // sw	a1,4(s7)
0x1000ff05,    // b	3e8 <$L42>
0x67de0004,    // daddiu	s8,s8,4

// 00000000000007d8 <$L185>:
0x0062302a,    // slt	a2,v1,v0
0x14c0ff18,    // bnez	a2,440 <$L225>
0x66b5fffc,    // daddiu	s5,s5,-4
0x1000fff0,    // b	7a8 <$L245>
0xaec20000,    // sw	v0,0(s6)

// 00000000000007ec <$L181>:
0x0062302a,    // slt	a2,v1,v0
0x14c0feb7,    // bnez	a2,2d0 <$L216>
0x6508fffc,    // daddiu	a4,a4,-4
0x1000fede,    // b	374 <$L246>
0xad820000,    // sw	v0,0(t0)

// 0000000000000800 <$L14>:
0x54c0fe54,    // bnezl	a2,154 <$L210>
0x008a102b,    // sltu	v0,a0,a6
0x1000fe72,    // b	1d4 <$L201>
0x654afffc,    // daddiu	a6,a6,-4
};

void initcode() {
    code[0x1c9] |= (uint32_t)((uintptr_t)code >> 2) & 0x3ffffff;
}

/*
void quicksort(int *l, int *r) {
    int *i = l, *j = r;
    int k = *(l + (r - l) / 2);
    for (;i <= j;) {
        while (*i < k) i++;
        while (*j > k) j--;
        if (i <= j) {
            int t = *i;
            *i = *j;
            *j = t;
            i++;
            j--;
        }
    }
    if (l < j) quicksort(l, j);
    if (i < r) quicksort(i, r);
}
*/
