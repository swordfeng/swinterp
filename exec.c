#include <alloca.h>
#include <inttypes.h>
#include "cpu.h"
#include "util.h"
#include "opcode.h"

#define opcode(instr) ((uint32_t)instr >> 26)
#define rs(instr) (((uint32_t)instr >> 21) & 0x1f)
#define rt(instr) (((uint32_t)instr >> 16) & 0x1f)
#define rd(instr) (((uint32_t)instr >> 11) & 0x1f)
#define shamt(instr) (((uint32_t)instr >> 6) & 0x1f)
#define funct(instr) ((uint32_t)instr & 0x3f)
#define imm(instr) ((uintptr_t)((intptr_t)((int16_t)((uint32_t)instr & 0xffff))))
#define addr(instr) ((uint32_t)instr & 0x3ffffff)

#if defined(__x86_64__)
#define read_sp(sp) asm volatile ("mov %%rsp, %0" : "=r"(sp))
#define write_sp(sp) asm volatile ("mov %0, %%rsp" : : "r"(sp))
#else
#define read_sp(sp) asm volatile ("mov %%esp, %0" : "=r"(sp))
#define write_sp(sp) asm volatile ("mov %0, %%esp" : : "r"(sp))
#endif

thread_local unsigned long reg[32];

unsigned long *get_reg() {
    return reg;
}

unsigned long exec(void *entry) {
    uint32_t *pc;
    uint32_t *pc_delay;
    uint32_t instr;
    // copy frame pointer
    read_sp(reg_sp);
    // zero zero register
    reg[0] = 0;
    // init pc to entry
    pc = entry;
    pc_delay = pc + 1;
    // guard for return address
    reg[31] = 0xdeadbeef;
    // start loop
    for (;(unsigned long)pc != 0xdeadbeef;) {
        instr = *pc;
        pc = pc_delay++;
        switch (opcode(instr)) {
            default: die("unknown opcode %x\n", opcode(instr));
            case OPCODE_SPECIAL:
                switch (funct(instr)) {
                    default: die("unknown special func %x\n", funct(instr));
                    case SPECIAL_SLL:
                        reg[rd(instr)] = sign_extend(reg[rt(instr)] << shamt(instr));
                        break;
                    case SPECIAL_JR:
                        pc_delay = (void *)reg[rs(instr)];
                        break;
                    case SPECIAL_ADD:
                    case SPECIAL_ADDU:
                        reg[rd(instr)] = sign_extend(reg[rs(instr)] + reg[rt(instr)]);
                        break;
                    case SPECIAL_DADD:
                    case SPECIAL_DADDU:
                        __GUARD_64__
                        reg[rd(instr)] = reg[rs(instr)] + reg[rt(instr)];
                        break;
                    case SPECIAL_DSUB:
                    case SPECIAL_DSUBU:
                        __GUARD_64__
                        reg[rd(instr)] = reg[rs(instr)] - reg[rt(instr)];
                        break;
                    case SPECIAL_SLT:
                        reg[rd(instr)] = (long)reg[rs(instr)] < (long)reg[rt(instr)];
                        break;
                    case SPECIAL_SLTU:
                        reg[rd(instr)] = reg[rs(instr)] < reg[rt(instr)];
                        break;
                    case SPECIAL_DSLL:
                        __GUARD_64__
                        reg[rd(instr)] = reg[rt(instr)] << shamt(instr);
                        break;
                    case SPECIAL_DSRA:
                        __GUARD_64__
                        reg[rd(instr)] = (uint64_t)((int64_t)reg[rt(instr)] >> shamt(instr));
                        break;
                    case SPECIAL_DSRL32:
                        __GUARD_64__
                        reg[rd(instr)] = (uint64_t)reg[rt(instr)] >> (shamt(instr) + 32);
                        break;
                }
                break;
            case OPCODE_JAL:
                reg[31] = (uintptr_t)pc_delay;
                pc_delay = (void *)(((uintptr_t)pc & ~0xfffffffUL) | (addr(instr) << 2));
                break;
            case OPCODE_BEQ:
                if (reg[rs(instr)] == reg[rt(instr)]) {
                    pc_delay = pc + imm(instr);
                }
                break;
            case OPCODE_BNE:
                if (reg[rs(instr)] != reg[rt(instr)]) {
                    pc_delay = pc + imm(instr);
                }
                break;
            case OPCODE_BEQL:
                if (reg[rs(instr)] == reg[rt(instr)]) {
                    pc_delay = pc + imm(instr);
                } else {
                    pc = pc_delay++;
                }
                break;
            case OPCODE_BNEL:
                if (reg[rs(instr)] != reg[rt(instr)]) {
                    pc_delay = pc + imm(instr);
                } else {
                    pc = pc_delay++;
                }
                break;
            case OPCODE_ADDI:
            case OPCODE_ADDIU:
                reg[rt(instr)] = sign_extend(reg[rs(instr)] + imm(instr));
                break;
            case OPCODE_DADDI:
            case OPCODE_DADDIU:
                __GUARD_64__
                reg[rt(instr)] = reg[rs(instr)] + imm(instr);
                break;
            case OPCODE_LW:
                reg[rt(instr)] = sign_extend(*(uint32_t *)(reg[rs(instr)] + imm(instr)));
                break;
            case OPCODE_SW:
                *(uint32_t *)(reg[rs(instr)] + imm(instr)) = (uint32_t)reg[rt(instr)];
                break;
            case OPCODE_LD:
                __GUARD_64__
                reg[rt(instr)] = *(uint64_t *)(reg[rs(instr)] + imm(instr));
                break;
            case OPCODE_SD:
                __GUARD_64__
                *(uint64_t *)(reg[rs(instr)] + imm(instr)) = reg[rt(instr)];
                break;
        }
    }
out:
    // sync frame pointer
    write_sp(reg_sp);
    return reg[2]; // $v0
}
