
#ifndef _WIN32
#define thread_local __thread
#else
#define thread_local __declspec(thread)
#endif

#define reg_sp (reg[29])

unsigned long exec(void *entry);
unsigned long *get_reg();
