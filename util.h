#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define die(...) do { \
    printf("swinterp: "); \
    printf(__VA_ARGS__); \
    exit(1); \
} while (0)

#if __WORDSIZE == 64
//defined(__LP64__) && __LP64__
#define __GUARD_64__
#define sign_extend(x) ((uintptr_t)(int64_t)((int32_t)(x)))
#else
#define __GUARD_64__ die("64bits not supported!\n");
#define sign_extend(x) (x)
#endif
