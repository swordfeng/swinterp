#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "cpu.h"
/*
void quicksort(int *l, int *r) {
    int *i = l, *j = r;
    int k = *(l + (r - l) / 2);
    for (;i <= j;) {
        while (*i < k) i++;
        while (*j > k) j--;
        if (i <= j) {
            int t = *i;
            *i = *j;
            *j = t;
            i++;
            j--;
        }
    }
    if (l < j) quicksort(l, j);
    if (i < r) quicksort(i, r);
}
*/
extern uint32_t code[];
void initcode();

int main() {
    static int arr[10000000];
    time_t start, end;
    srand(123456);
    for (int i = 0; i < 10000000; i++) arr[i] = rand();
    initcode();
    printf("ready\n");
    start = clock();
    //quicksort(arr, arr + 10000000);
    get_reg()[4] = (uintptr_t)arr;
    get_reg()[5] = (uintptr_t)(arr + 10000000);
    exec(code);
    end = clock();
    printf("done, time = %f\n", (double)(end - start) / CLOCKS_PER_SEC * 1000);
    for (int i = 0; i < 10000000; i += 100000) printf("%d\n", arr[i]);
    return 0;
}
